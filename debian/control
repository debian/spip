Source: spip
Section: web
Priority: optional
Maintainer: David Prévot <taffit@debian.org>
Build-Depends: cssmin,
               debhelper-compat (= 13),
               dh-apache2,
               dh-sequence-phpcomposer,
               minify,
               php-algo26-idna-convert,
               php-symfony-deprecation-contracts,
               phpab,
               uglifyjs
Homepage: https://www.spip.net/
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/debian/spip.git
Vcs-Browser: https://salsa.debian.org/debian/spip
Rules-Requires-Root: no

Package: spip
Architecture: all
Depends: fonts-dustin,
         libjs-jquery,
         libjs-jquery-form,
         libjs-jquery-jstree,
         libjs-mediaelement,
         libjs-prefix-free,
         libjs-twitter-bootstrap-datepicker,
         node-js-cookie,
         php-getid3,
         php-mysql | php-pgsql | php-sqlite3,
         ${misc:Depends},
         ${phpcomposer:Debian-require}
Recommends: default-mysql-server | virtual-mysql-server | postgresql,
            imagemagick | netpbm,
            php-sqlite3,
            ${misc:Recommends}
Suggests: ${phpcomposer:Debian-suggest}
Description: website engine for publishing
 SPIP is a publishing system for the Internet in which great importance
 is attached to collaborative working, to multilingual environments,
 and to simplicity of use for web authors.
 .
 SPIP's benefit consists in:
 .
  * managing a magazine type site i.e. made up mainly of
    articles and news items inserted in an arborescence
    of sections nested in each others.
  * completely separating and distributing three kinds of tasks
    over various players: the graphic design, the site editorial
    input through the submission of articles and news items and
    the site editorial management.
  * spare the webmaster and all the participants to the life of
    the site, a number of tedious aspects of web publishing as
    well as the need to learn lengthy technical skills.
    SPIP allows you to start creating your sections and
    articles straight away.
