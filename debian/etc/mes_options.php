<?php
    require '/usr/share/spip/mutualisation/mutualiser.php';
    require '/etc/spip/multisite.php';
    $site = spip_get_site($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    define('_DIR_PLUGINS_SUPPL', _DIR_RACINE . "sites/$site/plugins/");
    define('_DIR_PLUGINS_AUTO', _DIR_RACINE . "sites/$site/plugins/auto/");
    demarrer_site($site
     // See https://contrib.spip.net/La-mutualisation-facile-modifications-manuelles
     // for complete parameters definitions
     ,array(
     //   'creer_site' => true,
     //   'creer_base' => true,
     //   'code' => 'plouf',
     //   Enable the following for having links to
     //   images as '/IMG/jpg/image.jpg' instead of
     //   '/sites/spip.domain.org/IMG/jpg/image.jpg'
          'url_img_courtes' => true,
     //   'creer_user_base' => true,
     //   'mail' => 'admmin@spip.domain.org'
         )   
     );
?>
