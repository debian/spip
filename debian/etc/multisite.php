<?php

// Custom include for spip multisite under debian

// First define a global variable for multiple sites:
$GLOBALS['spip_sites'] = array();

// Now include all defined sites:
$dir = "/etc/spip/sites";

// Including all enabled sites.
if ( is_dir( $dir )
   &&
     $dh = opendir( $dir ) ) {
        while ( ( $file = readdir( $dh ) ) !== false ) {
	    if ( preg_match( "/.php$/",$file ) && is_readable($dir . DIRECTORY_SEPARATOR . $file) ) {
                include_once( $dir . DIRECTORY_SEPARATOR . $file );
            }
        }
        closedir( $dh );
     }

// then a function for getting them:
function spip_get_site($query) {
  foreach ($GLOBALS['spip_sites'] as $name => $hosts) {
     foreach ($hosts as $def_host) {
       if (preg_match('/' . $def_host . '/',$query) >= 1) { return $name; }
     }
  }

  // If not matched, return default
  return 'default';

}

?>
